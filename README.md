# Update an Existing CloudFormation Stack
An example script and configuration for updating an existing [AWS CloudFormation](https://aws.amazon.com/cloudformation/) stack with Bitbucket Pipelines.  A sample CloudFormation template is included to use as a demo for trying out the code and configuration.

## How To Use It
* Optional:  Create a new [AWS CloudFormation stack](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/GettingStarted.html) using the `sample_cfn_stack.template` included in this repository as a demo.
    * Warning:  This template creates an Amazon DynamoDB table. You might be billed for the AWS resources used if you create a stack from this template.  Please understand your current usage and [DynamoDB pricing](https://aws.amazon.com/dynamodb/pricing/).
* Add the required Environment Variables below
* Copy `cfn_stack_update.py` to your project
    * When using this script, you must pass two arguments in order:
      * The name of the CFN stack you want to update
      * The location of the JSON template relative to the repository
* Copy `bitbucket-pipelines.yml` to your project
    * Or use your own, just be sure to include all steps in the sample yml file
* Copy `sample_cfn_stack.template` to your project if you would like to try this using a sample template
    * If you used this sample template to create the stack above you should modify some part of the template so an update to the stack is made.  For example change 'Default' value in 'ReadCapacityUnits' from 1 to 2.

## Required Environment Variables
  * `AWS_SECRET_ACCESS_KEY`:  Secret key for a user with the required permissions.
  * `AWS_ACCESS_KEY_ID`:  Access key for a user with the required permissions.
  * `AWS_DEFAULT_REGION`:  Region where the target AWS Lambda function is.

## Required Permissions in AWS
It is recommended you [create](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) a separate user account used for this deploy process.  This user should be associated with a group that has an attached policy with all permissions required to update the CloudFormation stack.  At a minimum, permissions required will be:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1462810127000",
            "Effect": "Allow",
            "Action": [
                "cloudformation:UpdateStack"
            ],
            "Resource": [
                "arn:aws:cloudformation:us-west-2:575330773084:stack/YOUR_STACK_NAME/*"
            ]
        }
    ]
}
```

Other permissions are required for each type of resource that is in the stack (e.g. EC2, DynamoDB, etc).  To update the included `sample_cfn_stack.template` you can also attach the `AmazonDynamoDBFullAccess` [AWS managed policy](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html) for the required permissions to update the DynamoDB table.  Note that the above permissions are more than what is required in a real scenario. For any real use, you should limit the access to just the AWS resources in your context.

# License
Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Note: Other license terms may apply to certain, identified software files contained within or distributed with the accompanying software if such terms are included in the directory containing the accompanying software. Such other license terms will then apply in lieu of the terms of the software license above.
